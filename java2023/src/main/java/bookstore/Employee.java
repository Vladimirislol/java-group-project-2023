package bookstore;

public class Employee {

    private double salary;
    private int employeeId;

    public Employee(double salary, int employeeId) {
        this.salary = salary;
        this.employeeId = employeeId;
    }

    public double getSalary() {
        return salary;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    // Adding products to the inventory
    public void addProduct(Product){

    }

    // Removing products to the inventory
    public void removeProduct(Product){

    }
}
