package bookstore;

public class Book extends Product {
    private String title;
    private Author author;

    public Book(String title, Author author) {
        this.title = title;
        this.author = new Author("", "");
    }

    public String getTitle() {
        return this.title;

    }

    public String getAuthor() {
        return this.author.getFirstname() + this.author.getLastname();
    }
}
