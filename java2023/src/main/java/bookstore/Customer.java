package bookstore;

public class Customer {

    private String email;
    private String phoneNumber;
    private int numberOfItems;

    public Customer(String email, String phoneNumber, int numberOfItems) {
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.numberOfItems = numberOfItems;
    }

    public String getEmail() {
        return email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public int getNumberOfItems() {
        return numberOfItems;
    }

}
